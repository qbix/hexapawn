import {BoardEntry} from './bot/Bot';
import {Piece} from './Piece';

export function compareBoardEntries(be1: BoardEntry, be2: BoardEntry): number {
    if (be1.getMoves().length > be2.getMoves().length) {
        return -1;
    } else if (be1.getMoves().length < be2.getMoves().length) {
        return 1;
    } else {
        return compareBoards(be1, be2);
    }
}

function compareBoards(be1: BoardEntry, be2: BoardEntry): number {
    const board1 = be1.getBoard().fields;
    const board2 = be2.getBoard().fields;
    for (let row = 0; row < board1.length; row++) {
        for (let column = 0; column < board1[row].length; column++) {
            const compFields = compareFields(
                board1[row][column],
                board2[row][column]
            );
            if (compFields != 0) {
                return compFields;
            }
        }
    }
    return 0;
}

function compareFields(piece1: (Piece | null), piece2: (Piece | null)): number {
    if (piece1 == piece2) {
        return 0;
    } else {
        const hier1 = hierarchy.indexOf(piece1);
        const hier2 = hierarchy.indexOf(piece2);
        if (hier1 >= hier2) {
            return -1;
        } else if (hier1 < hier2) {
            return 1;
        } else {
            return 0;
        }
    }
}

const hierarchy: (Piece | null)[] = [null, Piece.NOUGHT, Piece.CROSS];
