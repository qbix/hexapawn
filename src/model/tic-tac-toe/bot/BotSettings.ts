export default class BotSettings {
    get win(): number {
        return this._win;
    }

    get loss(): number {
        return this._loss;
    }

    get draw(): number {
        return this._draw;
    }

    private readonly _win: number;
    private readonly _loss: number;
    private readonly _draw: number;

    constructor(win: number, loss: number, draw: number) {
        this._win = win;
        this._loss = loss;
        this._draw = draw;
    }

    setWinReward(win: number): BotSettings {
        return new BotSettings(win, this.loss, this.draw);
    }

    setLossReward(loss: number): BotSettings {
        return new BotSettings(this.win, loss, this.draw);
    }

    setDrawReward(draw: number): BotSettings {
        return new BotSettings(this.win, this.loss, draw);
    }
}
