import Board from '../../shared/Board';
import {Piece} from '../Piece';
import Coordinates from '../../shared/Coordinates';

export default interface BotPlacement {
    board: Board<Piece>,
    move: Coordinates
}
