import Moves from '../Moves';
import BotPlacement from './BotPlacement';
import {Piece} from '../Piece';
import Board from '../../shared/Board';
import Coordinates from '../../shared/Coordinates';
import BotSettings from './BotSettings';
import {PlayerResult} from '../../shared/PlayerResult';

export default class Bot {
    get boardEntries(): BoardEntry[] {
        return this._boardEntries;
    }

    get settings(): BotSettings {
        return this._settings;
    }

    private _boardEntries: BoardEntry[] = [];
    private _settings: BotSettings = new BotSettings(1, -1, 0);

    makeMove(board: Board<Piece>, availableMoves: Moves): Coordinates {
        let boardEntry = this.getBoardEntry(board);
        if (!boardEntry) {
            boardEntry = this.registerBoard(board, availableMoves);
        }
        return boardEntry.pickMove();
    }

    private getBoardEntry(board: Board<Piece>): (BoardEntry | undefined) {
        return this._boardEntries
            .find(entry => entry.getBoard().equals(board));
    }

    private registerBoard(board: Board<Piece>, availebleMoves: Moves): BoardEntry {
        const boardEntry = new BoardEntry(
            board, availebleMoves
                .getAllMoves()
                .map(move => {
                    return {
                        move: move.coords,
                        ratio: 1
                    };
                }));
        this._boardEntries.push(boardEntry);
        return boardEntry;
    }

    learn(moves: BotPlacement[], result: PlayerResult): void {
        moves
            .forEach(mv => {
                const boardEntry = this._boardEntries.find(entry => entry.getBoard().equals(mv.board));
                if (boardEntry) {
                    const moveEntry = boardEntry.getMoves().find(m => m.move.equals(mv.move));
                    if (moveEntry) {
                        switch (result) {
                            case PlayerResult.WIN:
                                moveEntry.ratio += this._settings.win;
                                break;
                            case PlayerResult.LOSS:
                                moveEntry.ratio += this._settings.loss;
                                break;
                            case PlayerResult.DRAW:
                                moveEntry.ratio += this._settings.draw;
                                break;
                        }
                        if (moveEntry.ratio < 0) {
                            moveEntry.ratio = 0;
                        }
                    }
                    const total = boardEntry.getMoves().map(move => move.ratio).reduce((r1, r2) => r1 + r2);
                    if (total <= 0) {
                        boardEntry.getMoves().forEach(move =>
                            move.ratio = 1
                        );
                    }
                }
            });
    }

    setSettings(settings: BotSettings) {
        this._settings = settings;
    }
}

export class BoardEntry {
    private board: Board<Piece>;
    private moves: MoveEntry[];

    constructor(board: Board<Piece>, moves: MoveEntry[]) {
        this.board = board;
        this.moves = moves;
    }

    pickMove(): Coordinates {
        const movesTotal = this.moves.map(move => move.ratio).reduce((r1, r2) => r1 + r2);
        const pick = Math.random() * (movesTotal);
        let upToNow = 0;
        for (let moveEntry of this.moves) {
            upToNow += moveEntry.ratio;
            if (pick <= upToNow) {
                return moveEntry.move;
            }
        }
        return this.moves[this.moves.length - 1].move;
    }

    getBoard(): Board<Piece> {
        return this.board;
    }

    getMoves(): MoveEntry[] {
        return this.moves;
    }

}

export interface MoveEntry {
    move: Coordinates;
    ratio: number;
}

export const defaultBot = new Bot();
