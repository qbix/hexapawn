import {Piece} from './Piece';
import Moves from './Moves';
import {Result} from './Result';
import BotPlacement from './bot/BotPlacement';
import BaseGame from '../shared/BaseGame';
import Coordinates from '../shared/Coordinates';
import Placement from '../shared/Placement';

export default class Game extends BaseGame<Piece> {
    get nextPiece(): Piece {
        return this._nextPiece;
    }

    private _nextPiece: Piece;
    private readonly movements: Map<Piece, BotPlacement[]>;

    constructor() {
        super([
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ]);
        this._nextPiece = Piece.CROSS;
        this.movements = new Map<Piece, BotPlacement[]>();
        this.movements.set(Piece.NOUGHT, []);
        this.movements.set(Piece.CROSS, []);
    }

    move = (move: Coordinates): void => {
        this.storeMovement(move);
        this.atomicMove(move);
    };

    getMovements(piece: Piece): BotPlacement[] {
        const movements = this.movements.get(piece);
        if (movements) {
            return movements;
        } else {
            throw new Error('Missing movements for ' + piece);
        }
    }

    private storeMovement(move: Coordinates) {
        this.movements.get(this._nextPiece)?.push(
            {
                board: this.currentBoard,
                move: move
            }
        );
    }

    private atomicMove(move: Coordinates) {
        this.currentBoard = this.currentBoard.putPiece(new Placement<Piece>(this._nextPiece, move));
        this.switchPiece();
    }

    private switchPiece() {
        this._nextPiece = Game.oppositePiece(this._nextPiece);
    }

    private static oppositePiece(piece: Piece) {
        if (piece == Piece.NOUGHT) {
            return Piece.CROSS;
        } else {
            return Piece.NOUGHT;
        }
    }

    getAvailableMoves = (): Moves => {
        if (this.winner()) {
            return new Moves([]);
        }
        const moves: Placement<Piece>[] = [];
        const fields = this.currentBoard.fields;
        for (let row = 0; row < fields.length; row++) {
            for (let col = 0; col < fields[row].length; col++) {
                if (fields[row][col] == null) {
                    moves.push(new Placement<Piece>(this._nextPiece, new Coordinates(row, col)));
                }
            }
        }
        return new Moves(moves);
    };

    winner(): (Result.DRAW | Piece | undefined) {
        if (this.isAWinner(Piece.NOUGHT)) {
            return Piece.NOUGHT;
        } else if (this.isAWinner(Piece.CROSS)) {
            return Piece.CROSS;
        } else if (this.isAnyFieldLeft()) {
            return undefined;
        } else {
            return Result.DRAW;
        }
    }

    private isAWinner(piece: Piece): boolean {
        return this.hasFullRow(piece)
            || this.hasFullColumn(piece)
            || this.hasFullDiagonal(piece);

    }

    private hasFullRow(piece: Piece): boolean {
        return this.currentBoard.fields
            .filter(row => row
                .filter(p => p != piece)
                .length == 0
            ).length > 0;
    }

    private hasFullColumn(piece: Piece): boolean {
        const fields = this.currentBoard.fields;
        for (let col = 0; col < fields[0].length; col++) {
            let count = 0;
            for (let row = 0; row < fields.length; row++) {
                if (fields[row][col] == piece) {
                    count++;
                }
            }
            if (count == fields[0].length) {
                return true;
            }
        }
        return false;
    }

    private hasFullDiagonal(piece: Piece): boolean {
        let diag1 = 0;
        let diag2 = 0;
        const fields = this.currentBoard.fields;
        for (let row = 0; row < fields.length; row++) {
            for (let col = 0; col < fields[row].length; col++) {
                if (fields[row][col] == piece) {
                    if (row == col) {
                        diag1++;
                    }
                    if (row == fields[row].length - (col + 1)) {
                        diag2++;
                    }
                }
            }
        }
        return (diag1 == fields.length || diag2 == fields.length);
    }

    private isAnyFieldLeft(): boolean {
        const fieldsLeft = this.currentBoard.fields
            .reduce((row1, row2) => [...row1, ...row2])
            .filter(piece => piece == null);
        return fieldsLeft.length > 0;
    }
}
