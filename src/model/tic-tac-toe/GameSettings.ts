export default class GameSettings {
    get pauseAfterGame(): number {
        return this._pauseAfterGame;
    }

    get pauseAfterMove(): number {
        return this._pauseAfterMove;
    }

    get isSimulationRunning(): boolean {
        return this._isSimulationRunning;
    }

    get participant1(): Participant {
        return this._participant1;
    }

    get participant2(): Participant {
        return this._participant2;
    }

    constructor(pauseAfterGame: number, pauseAfterMove: number, isSimulationRunning: boolean, participant1: Participant, participant2: Participant) {
        this._pauseAfterGame = pauseAfterGame;
        this._pauseAfterMove = pauseAfterMove;
        this._isSimulationRunning = isSimulationRunning;
        this._participant1 = participant1;
        this._participant2 = participant2;
    }

    private _pauseAfterGame: number;
    private _pauseAfterMove: number;
    private _isSimulationRunning: boolean;
    private _participant1: Participant;
    private _participant2: Participant;

    public setPauseAfterGame(pause: number): GameSettings {
        return new GameSettings(pause, this.pauseAfterMove, this.isSimulationRunning, this.participant1, this.participant2);
    }

    public setPauseAfterMove(pause: number): GameSettings {
        return new GameSettings(this.pauseAfterGame, pause, this.isSimulationRunning, this.participant1, this.participant2);
    }

    public setSimulationRunning(running: boolean): GameSettings {
        return new GameSettings(this.pauseAfterGame, this.pauseAfterMove, running, this.participant1, this.participant2);
    }


    public setParticipant1(participant: Participant): GameSettings {
        return new GameSettings(this.pauseAfterGame, this.pauseAfterMove, this.isSimulationRunning, participant, this.participant2);
    }

    public setParticipant2(participant: Participant): GameSettings {
        return new GameSettings(this.pauseAfterGame, this.pauseAfterMove, this.isSimulationRunning, this.participant1, participant);
    }


}

export enum Participant {
    PLAYER = 'Player', BOT = 'Bot'
}
