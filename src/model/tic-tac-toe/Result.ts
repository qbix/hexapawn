export enum Result {
    PLAYER_ONE = 'Player One',
    PLAYER_TWO = 'Player Two',
    DRAW = 'Draw'
}
