import Coordinates from '../shared/Coordinates';
import Placement from '../shared/Placement';
import {Piece} from './Piece';

export default class Moves {
    private readonly moves: Array<Placement<Piece>>;

    constructor(moves: Array<Placement<Piece>>) {
        this.moves = moves;
    }

    getAllMoves(): Array<Placement<Piece>> {
        return this.moves;
    }

    hasCoordinates(coords: Coordinates): boolean {
        return undefined != this.moves
            .find(move => move.coords.equals(coords));
    }
}
