import {Result} from './Result';

export function total(history: Result[], result: Result): number {
    if (history.length == 0) {
        return 0;
    }
    const count = history.filter(res => res == result).length;
    return count / history.length;
}

export function chartData(history: Result[]): any[] {
    const data: any[] = [];
    for (let i = 0; i < history.length; i++) {
        const subHistory = subChart(history, i + 1);
        const player = subHistory
            .filter(res => res == Result.PLAYER_ONE)
            .length;
        const bot = subHistory
            .filter(res => res == Result.PLAYER_TWO)
            .length;
        const draw = subHistory
            .filter(res => res == Result.DRAW)
            .length;
        const total = subHistory.length;
        data.push({
            n: i,
            'Player': player / total,
            'Draw': draw / total,
            'Bot': bot / total
        });
    }
    return data;
}

function subChart(history: Result[], size: number) {
    let start = size - 20;
    if (start < 0) {
        start = 0;
    }
    return history
        .slice(start, size);
}
