import Coordinates from './Coordinates';

export default class Placement<Piece> {

    get piece(): Piece {
        return this._piece;
    }

    get coords(): Coordinates {
        return this._coords;
    }

    private readonly _piece: Piece;
    private readonly _coords: Coordinates;

    constructor(piece: Piece, coords: Coordinates) {
        this._piece = piece;
        this._coords = coords;
    }

    equals(placement: Placement<Piece>): boolean {
        return this.piece == placement.piece && this.coords.equals(placement.coords);
    }
}
