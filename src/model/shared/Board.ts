import Coordinates from './Coordinates';
import Movement from './Movement';
import Placement from './Placement';

export default class Board<Piece> {
    private readonly _fields: (Piece | null)[][];
    get fields(): (Piece | null)[][] {
        return this._fields;
    }

    constructor(fields: (Piece | null)[][]) {
        this._fields = fields;
    }

    putPiece(placement: Placement<Piece>): Board<Piece> {
        const newFields = this.copyFields();
        newFields[placement.coords.row][placement.coords.column] = placement.piece;
        return new Board<Piece>(newFields);
    }

    takePiece(coords: Coordinates): Board<Piece> {
        const newFields = this.copyFields();
        newFields[coords.row][coords.column] = null;
        return new Board<Piece>(newFields);
    }

    movePiece(movement: Movement): Board<Piece> {
        const piece = this.getPiece(movement.from);
        if (piece) {
            return this
                .putPiece(new Placement<Piece>(piece, movement.to))
                .takePiece(movement.from);
        } else {
            throw new Error('Cannot move a piece from empty space');
        }
    }

    getPiece(coords: Coordinates): (Piece | null) {
        return this._fields[coords.row][coords.column];
    }

    equals(board: Board<Piece>): boolean {
        for (let row = 0; row < this._fields.length; row++) {
            for (let column = 0; column < this._fields[row].length; column++) {
                const coords = new Coordinates(row, column);
                if (this.getPiece(coords) != board.getPiece(coords)) return false;
            }
        }
        return true;
    }

    private copyFields() {
        const newFields = [];
        for (let row of this._fields) {
            newFields.push([...row]);
        }
        return newFields;
    }
}
