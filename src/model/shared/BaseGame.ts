import Board from './Board';

export default class BaseGame<Piece> {
    protected currentBoard: Board<Piece>;

    constructor(fields: (Piece | null)[][]) {
        this.currentBoard = new Board<Piece>(fields);
    }

    getCurrentBoard(): Board<Piece> {
        return new Board<Piece>(this.currentBoard.fields);
    }
}
