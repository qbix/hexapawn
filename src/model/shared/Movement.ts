import Coordinates from './Coordinates';

export default class Movement {
    get to(): Coordinates {
        return this._to;
    }

    get from(): Coordinates {
        return this._from;
    }

    private readonly _from: Coordinates;
    private readonly _to: Coordinates;

    constructor(from: Coordinates, to: Coordinates) {
        this._from = from;
        this._to = to;
    }

    equals(movement: Movement) {
        return this.from.equals(movement.from) && this.to.equals(movement.to);
    }
}
