export function format(ratio: number): string {
    return `${Math.round(ratio * 10000) / 100}%`;
}

export function rgb(ratio: number): string {
    const rMin = 62;
    const gMin = 221;
    const bMin = 112;
    const rMax = 221;
    const gMax = 94;
    const bMax = 62;
    var r = rMin + ((1 - ratio) * (rMax - rMin));
    var g = gMin + ((1 - ratio) * (gMax - gMin));
    var b = bMin + ((1 - ratio) * (bMax - bMin));
    return `rgb(${r}, ${g}, ${b})`;
}
