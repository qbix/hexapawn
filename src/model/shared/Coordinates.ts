export default class Coordinates {
    get column(): number {
        return this._column;
    }

    get row(): number {
        return this._row;
    }

    private readonly _row: number;
    private readonly _column: number;

    constructor(row: number, column: number) {
        this._row = row;
        this._column = column;
    }

    equals(coords?: Coordinates) {
        if (coords == null) {
            return false;
        } else {
            return this.row == coords.row && this.column == coords.column;
        }
    }
}
