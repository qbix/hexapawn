export default class Coordinates {
    private readonly x: number;
    private readonly y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    equals = (other?: Coordinates): boolean => {
        if (!other) {
            return false;
        }
        return this.x === other.x && this.y === other.y;
    };

    getX = (): number => {
        return this.x;
    };

    getY = (): number => {
        return this.y;
    };
}
