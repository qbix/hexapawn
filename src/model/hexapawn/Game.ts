import {Piece} from './Piece';
import Moves from './Moves';
import Bot from './bot/Bot';
import BotMovement from './bot/BotMovement';
import BaseGame from '../shared/BaseGame';
import Coordinates from '../shared/Coordinates';
import Movement from '../shared/Movement';

export default class Game extends BaseGame<Piece> {
    currentColor: Piece;
    availableMoves: Moves;
    bot: Bot;
    botMovements: BotMovement[];

    constructor(bot: Bot) {
        super([
            [Piece.BLACK, Piece.BLACK, Piece.BLACK],
            [null, null, null],
            [Piece.WHITE, Piece.WHITE, Piece.WHITE]
        ]);
        this.currentColor = Piece.WHITE;
        this.availableMoves = this.findAvailableMoves();
        this.bot = bot;
        this.botMovements = [];
    }

    findAvailableMoves = (): Moves => {
        const origins = this.getMovable();
        const moves = new Map<Coordinates, Coordinates[]>();
        for (let origin of origins) {
            moves.set(origin, this.getAvailable(origin));
        }
        return new Moves(moves);
    };

    getMovable = (): Coordinates[] => {
        const result: Coordinates[] = [];
        for (let x = 0; x < this.currentBoard.fields[0].length; x++) {
            for (let y = 0; y < this.currentBoard.fields.length; y++) {
                if (this.fieldAt(new Coordinates(x, y)) == this.currentColor) {
                    result.push(new Coordinates(x, y));
                }
            }
        }
        return result;
    };

    getAvailable = (coords: Coordinates): Coordinates[] => {
        if (!this.fieldAt(coords)) {
            return [];
        }
        const options = [] as Coordinates[];
        const freeSpaceInFront = this.getFreeSpaceInFront(coords);
        if (freeSpaceInFront) {
            options.push(freeSpaceInFront);
        }
        options.push(...this.availableCaptures(coords));
        return options;
    };

    getFreeSpaceInFront = (coords: Coordinates): (Coordinates | null) => {
        const rowDirection = this.rowDirection();
        const newCoords = new Coordinates(coords.row + rowDirection, coords.column);
        if (!this.fieldAt(newCoords)) {
            return newCoords;
        } else {
            return null;
        }
    };

    availableCaptures = (coords: Coordinates): Coordinates[] => {
        if (!this.fieldAt(coords)) {
            return [];
        }
        const options = [];
        const opositeColor = this.fieldAt(coords) == Piece.BLACK ? Piece.WHITE : Piece.BLACK;
        const rowDirection = this.rowDirection();
        if (this.fieldAt(new Coordinates(coords.row + rowDirection, coords.column - 1)) == opositeColor) {
            options.push(new Coordinates(coords.row + rowDirection, coords.column - 1));
        }
        if (this.fieldAt(new Coordinates(coords.row + rowDirection, coords.column + 1)) == opositeColor) {
            options.push(new Coordinates(coords.row + rowDirection, coords.column + 1));
        }
        return options;
    };

    rowDirection = (): number => {
        if (this.currentColor == Piece.BLACK) {
            return 1;
        } else {
            return -1;
        }
    };

    move = (move: Movement): Game => {
        this.atomicMove(move);
        if (!this.getWinner()) {
            const botMove = this.bot.makeMove(this.currentBoard, this.availableMoves);
            this.botMovements.push({
                board: this.currentBoard,
                move: botMove
            });
            this.atomicMove(botMove);
        }
        if (this.getWinner()) {
            this.availableMoves = new Moves(new Map<Coordinates, Coordinates[]>());
        }
        return this;
    };

    atomicMove = (move: Movement) => {
        this.currentBoard = this.currentBoard.movePiece(move);
        this.currentColor = this.opositeColor();
        this.availableMoves = this.findAvailableMoves();
    };

    opositeColor = (): Piece => {
        return this.currentColor == Piece.WHITE ? Piece.BLACK : Piece.WHITE;
    };
    fieldAt = (coords: Coordinates): (Piece | null) => {
        if (!this.currentBoard.fields[coords.row]) {
            return null;
        }
        if (!this.currentBoard.fields[coords.row][coords.column]) {
            return null;
        }
        return this.currentBoard.fields[coords.row][coords.column];
    };

    getWinner = (): (Piece | null) => {
        if (this.pawnAtOpositeEnd()) {
            return this.pawnAtOpositeEnd();
        }
        if (this.currentPlayerHasNoMoves()) {
            return this.opositeColor();
        }
        return null;
    };

    pawnAtOpositeEnd(): (Piece | null) {
        if (this.whiteAtBlackEnd()) {
            return Piece.WHITE;
        }
        if (this.blackAtWhiteEnd()) {
            return Piece.BLACK;
        }
        return null;
    }

    whiteAtBlackEnd = (): boolean => {
        return this.currentBoard.fields[0]
            .filter(field => field == Piece.WHITE)
            .length > 0;
    };

    blackAtWhiteEnd = (): boolean => {
        return this.currentBoard.fields[2]
            .filter(field => field == Piece.BLACK)
            .length > 0;
    };

    currentPlayerHasNoMoves = (): boolean => {
        return this.availableMoves.getAllMoves().length == 0;
    };
}
