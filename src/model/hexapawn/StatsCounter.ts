import {Piece} from './Piece';

export const getTotal = (history: Piece[], color: Piece) => {
    return history
        .filter(field => field == color)
        .length;
};

export const getTotalRatio = (history: Piece[]) => {
    if (history.length == 0) return 0;
    const white = getTotal(history, Piece.WHITE);
    const black = getTotal(history, Piece.BLACK);
    return white / (white + black);
};

export const getMoving10Average = (history: Piece[]): any[] => {
    const result: any[] = [];
    for (let i = 0; i < history.length; i++) {
        result.push({
            n: i,
            avg10: getTotalRatio(getWithPrevious(history, i, 10)) * 100,
            avg20: getTotalRatio(getWithPrevious(history, i, 20)) * 100,
            avgTotal: getTotalRatio(getWithPrevious(history, i, history.length)) * 100
        });
    }
    return result;
};

export const getWithPrevious = (history: Piece[], index: number, size: number): Piece[] => {
    if (history.length == 0) return [];
    return history.filter((field, idx) => {
        return (idx > index - size) && idx <= index;
    });
};

export const getLast = (history: Piece[], size: number): Piece[] => {
    return getWithPrevious(history, history.length - 1, size);
};

export const currentStreak = (history: Piece[]) => {
    if (history.length == 0) {
        return 'No data';
    } else {
        const winner = history[history.length - 1];
        let index = history.length - 1;
        let count = 0;
        while (history[index] == winner) {
            count++;
            index--;
        }
        return `${winner} - ${count}`;
    }
};
