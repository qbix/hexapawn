export default class BotSettings {
    get reward(): number {
        return this._reward;
    }

    get punishment(): number {
        return this._punishment;
    }

    get symmetry(): boolean {
        return this._symmetry;
    }

    private readonly _reward: number;
    private readonly _punishment: number;
    private readonly _symmetry: boolean;

    constructor(reward: number, punishment: number, symmetry: boolean) {
        this._reward = reward;
        this._punishment = punishment;
        this._symmetry = symmetry;
    }

    setReward(reward: number) {
        return new BotSettings(reward, this.punishment, this.symmetry);
    }

    setPunishment(punishment: number) {
        return new BotSettings(this.reward, punishment, this.symmetry);
    }

    setSymmetry(symmetry: boolean): BotSettings {
        return new BotSettings(this.reward, this.punishment, symmetry);
    }

    toggleSymmetry(): BotSettings {
        return new BotSettings(this.reward, this.punishment, !this.symmetry);
    }
}
