import Moves from '../Moves';
import BotMovement from './BotMovement';
import {Piece} from '../Piece';
import Board from '../../shared/Board';
import Movement from '../../shared/Movement';
import Coordinates from '../../shared/Coordinates';
import BotSettings from './BotSettings';

export default class Bot {
    get boardEntries(): BoardEntry[] {
        return this._boardEntries;
    }

    set settings(value: BotSettings) {
        this._settings = value;
    }

    get settings(): BotSettings {
        return this._settings;
    }

    private _boardEntries: BoardEntry[] = [];
    private _settings: BotSettings = new BotSettings(1, 1, true);

    makeMove = (currentBoard: Board<Piece>, availableMoves: Moves): Movement => {
        let boardEntry = this.getBoardEntry(currentBoard);
        if (!boardEntry) {
            boardEntry = this.registerBoardEntry(currentBoard, availableMoves);
        }
        return boardEntry.pickMove();
    };

    getBoardEntry = (board: Board<Piece>): (BoardEntry | undefined) => {
        return this._boardEntries
            .find(entry => entry.board.equals(board));
    };

    registerBoardEntry = (board: Board<Piece>, availableMoves: Moves): BoardEntry => {
        let entry: BoardEntry;
        if (this._settings.symmetry) {
            const mirrorBoard = this.mirrorBoard(board);
            const mirror = this.getBoardEntry(this.mirrorBoard(board));
            if (mirror) {
                entry = new BoardEntry(board, this.mirrorMoves(mirror.moves.map(move => move.move)));
            } else {
                entry = new BoardEntry(board, availableMoves.getAllMoves());
                if (!mirrorBoard.equals(board)) {
                    this._boardEntries.push(new BoardEntry(mirrorBoard, this.mirrorMoves(availableMoves.getAllMoves())));
                }
            }
        } else {
            entry = new BoardEntry(board, availableMoves.getAllMoves());
        }
        this._boardEntries.push(entry);
        return entry;
    };

    learn = (movements: BotMovement[], winner: Piece): void => {
        for (let mv of movements) {
            this.learnBoard(mv, winner);
            if (this._settings.symmetry) {
                const mirror = this.mirrorBoard(mv.board);
                if (!mirror.equals(mv.board)) {
                    this.learnBoard({
                        board: mirror,
                        move: this.mirrorMove(mv.move)
                    }, winner);
                } else {
                    this.learnBoard({
                        board: mv.board,
                        move: this.mirrorMove(mv.move)
                    }, winner);
                }
            }
        }
    };

    learnBoard = (mv: BotMovement, winner: Piece) => {
        let boardEntry = this.getBoardEntry(mv.board);
        if (!boardEntry) {
            return;
        }
        const moveEntry = boardEntry.moves.find(move => move.move.equals(mv.move));
        if (!moveEntry) {
            return;
        }
        if (winner == Piece.WHITE) {
            moveEntry.ratio -= this._settings.punishment;
            if (moveEntry.ratio < 0) moveEntry.ratio = 0;
        } else {
            moveEntry.ratio += this._settings.reward;
            if (moveEntry.ratio < 0) moveEntry.ratio = 0;
        }
        if (boardEntry.moves.map(mv => mv.ratio).reduce((r1, r2) => r1 + r2) <= 0) {
            boardEntry.moves.forEach(mv => mv.ratio = 1);
        }
    };

    mirrorBoard(board: Board<Piece>): Board<Piece> {
        const fields = [] as (Piece | null)[][];
        for (let i = 0; i < board.fields.length; i++) {
            fields[i] = [];
            for (let j = 0; j < board.fields[i].length; j++) {
                fields[i].push(board.fields[i][board.fields[i].length - (j + 1)]);
            }
        }
        return new Board<Piece>(fields);
    }

    mirrorMoves = (moves: Movement[]): Movement[] => {
        return moves.map(
            move => this.mirrorMove(move)
        );
    };

    mirrorMove = (move: Movement): Movement => {
        return new Movement(
            this.mirrorCoordinates(move.from),
            this.mirrorCoordinates(move.to)
        );
    };

    mirrorCoordinates = (coords: Coordinates): Coordinates => {
        return new Coordinates(coords.row, 2 - coords.column);
    };
}

export const defaultBot = new Bot();

export class BoardEntry {
    board: Board<Piece>;
    moves: MoveEntry[];

    constructor(board: Board<Piece>, moves: Movement[]) {
        this.board = board;
        this.moves = moves
            .map(move => {
                return {
                    move: move,
                    ratio: 1
                };
            });
    }

    pickMove = (): Movement => {
        const movesTotal = this.moves.map(move => move.ratio).reduce((r1, r2) => r1 + r2);
        const pick = Math.random() * (movesTotal);
        let upToNow = 0;
        for (let moveEntry of this.moves) {
            upToNow += moveEntry.ratio;
            if (pick <= upToNow) {
                return moveEntry.move;
            }
        }
        return this.moves[this.moves.length - 1].move;
    };
}

export interface MoveEntry {
    move: Movement;
    ratio: number;
}
