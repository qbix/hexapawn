import Board from '../../shared/Board';
import {Piece} from '../Piece';
import Movement from '../../shared/Movement';

export default interface BotMovement {
    board: Board<Piece>;
    move: Movement;
}
