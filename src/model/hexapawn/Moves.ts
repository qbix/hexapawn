
import Movement from '../shared/Movement';
import Coordinates from '../shared/Coordinates';

export default class Moves {
    moves: Map<Coordinates, Coordinates[]> = new Map<Coordinates, Coordinates[]>();

    constructor(moves: Map<Coordinates, Coordinates[]>) {
        this.moves = moves;
    }


    getAllMoves(): Movement[] {
        const moves = [] as Movement[];
        this.moves.forEach(((value, key) => {
            moves.push(...value.map(val => new Movement(key, val)));
        }));
        return moves;
    }

    getMovesFrom(from: Coordinates): Movement[] {
        const key = Array.from(this.moves.keys())
            .find(key => key.equals(from));
        if (!key) {
            return [];
        }
        const destinations = this.moves.get(key);
        if (!destinations) {
            return [];
        } else {
            return destinations
                .map(to => new Movement(from, to));
        }
    }

    getValidOrigins(): Coordinates[] {
        return Array.from(this.moves.keys());
    }
}
