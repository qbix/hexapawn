import React, {Component, ReactNode} from 'react';
import FieldView from './FieldView';
import Moves from '../../model/tic-tac-toe/Moves';
import {MoveEntry} from '../../model/tic-tac-toe/bot/Bot';
import Board from '../../model/shared/Board';
import {Piece} from '../../model/tic-tac-toe/Piece';
import Coordinates from '../../model/shared/Coordinates';

export default class BoardView extends Component<Props, any> {
    render() {
        return (
            <div className="board-view">
                {this.fields()}
            </div>
        );
    }

    fields = (): ReactNode => {
        const fields = this.props.board.fields;
        const fieldViews: ReactNode[] = [];
        for (let row = 0; row < fields.length; row++) {
            for (let column = 0; column < fields[row].length; column++) {
                const coords = new Coordinates(row, column);
                fieldViews.push(<FieldView key={Math.random()}
                                           piece={fields[row][column]}
                                           probability={this.getProbability(coords)}
                                           onClick={this.handleClick(coords)}
                />);
            }
        }
        return <div className="fields">
            {fieldViews}
        </div>;
    };

    handleClick = (coords: Coordinates) => (): void => {
        if (this.props.availableMoves.hasCoordinates(coords)) {
            this.props.moveHandler(coords);
        }
    };

    private getProbability(coords: Coordinates): (number | undefined) {
        if (!this.props.probabilities || this.props.probabilities.length == 0) {
            return undefined;
        }
        const ratio = this.props.probabilities
            ?.find(entry => entry.move.equals(coords))?.ratio;

        const total = this.props.probabilities?.map(entry => entry.ratio)
            .reduce((r1, r2) => r1 + r2);
        if (ratio == undefined || total == undefined) {
            return undefined;
        } else {
            return ratio / total;
        }
    }

}

interface Props {
    board: Board<Piece>;
    availableMoves: Moves;
    moveHandler: (move: Coordinates) => void;
    probabilities?: MoveEntry[];
}
