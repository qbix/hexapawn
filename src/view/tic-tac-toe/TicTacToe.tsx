import React, {Component} from 'react';
import './TicTacToe.scss';
import BoardView from './BoardView';
import Game from '../../model/tic-tac-toe/Game';
import {Button} from '@mui/material';
import Bot from '../../model/tic-tac-toe/bot/Bot';
import {Result} from '../../model/tic-tac-toe/Result';
import StatisticsView from './StatisticsView';
import Coordinates from '../../model/shared/Coordinates';
import BotSettings from '../../model/tic-tac-toe/bot/BotSettings';
import {Piece} from '../../model/tic-tac-toe/Piece';
import {PlayerResult} from '../../model/shared/PlayerResult';
import {wait} from '@testing-library/user-event/dist/utils';
import SettingsView from './SettingsView';
import GameSettings, {Participant} from '../../model/tic-tac-toe/GameSettings';
import GameSettingsView from './GameSettingsView';

export default class TicTacToe extends Component<any, State> {
    state = {
        bot1: new Bot(),
        bot2: new Bot(),
        game: new Game(),
        history: [] as Result[],
        gameSettings: new GameSettings(
            1000,
            100,
            true,
            Participant.PLAYER, Participant.BOT)
    };

    render() {
        return <div className="tic-tac-toe">
            <span className="statistics">
                <StatisticsView history={this.state.history}/>
            </span>
            <span className="board">
                <BoardView
                    board={this.state.game.getCurrentBoard()}
                    availableMoves={this.state.game.getAvailableMoves()}
                    moveHandler={this.moveHandler}/>
                {this.state.game.winner() && <div>Result: {this.state.game.winner()}
                    <Button onClick={this.resetGame}>New game</Button>
                </div>}
            </span>
            <span className="bot-view">
                <SettingsView
                    gameSettings={this.state.gameSettings}
                    bot1Settings={this.state.bot1.settings}
                    bot2Settings={this.state.bot2.settings}
                    bot1BoardEntries={this.state.bot1.boardEntries}
                    bot2BoardEntries={this.state.bot2.boardEntries}
                    bot1SettingsChangeHandler={this.botSettingsChange(this.state.bot1)}
                    bot2SettingsChangeHandler={this.botSettingsChange(this.state.bot2)}
                    gameSettingsChangeHandler={this.gameSettingsChangeHandler}
                />
            </span>
        </div>;
    }

    botSettingsChange = (bot: Bot) => (settings: BotSettings): void => {
        bot.setSettings(settings);
        this.setState({});
    };

    moveHandler = (move: Coordinates): void => {
        this.state.game.move(move);
        const history: Result[] = [...this.state.history];
        const winner = this.state.game.winner();
        if (winner) {
            this.handleFinish(winner);
        } else {
            this.setAsyncState({})
                .then(this.proceedGame);
        }
    };

    handleFinish(winner: (Piece | Result.DRAW)) {
        const result = this.toPlayerResult(winner, Piece.CROSS);
        if (this.state.gameSettings.participant1 != Participant.PLAYER) {
            this.state.bot2.learn(this.state.game.getMovements(Piece.CROSS), result);
        }
        if (this.state.gameSettings.participant2 != Participant.PLAYER) {
            this.state.bot1.learn(this.state.game.getMovements(Piece.NOUGHT),
                this.toPlayerResult(winner, Piece.NOUGHT));
        }
        this.setAsyncState({history: [...this.state.history, this.toResult(winner)]})
            .then(() => wait(this.state.gameSettings.pauseAfterGame))
            .then(() => {
                if (this.state.gameSettings.participant1 == Participant.BOT
                    && this.state.gameSettings.isSimulationRunning
                ) {
                    this.resetGame();
                }
            });
    }

    proceedGame = () => {
        if (this.state.gameSettings.isSimulationRunning) {
            if (this.state.game.nextPiece == Piece.CROSS
                && this.state.gameSettings.participant1 == Participant.BOT) {
                wait(this.state.gameSettings.pauseAfterMove)
                    .then(() => this.makeBotMove(this.state.bot2));
            } else if (this.state.game.nextPiece == Piece.NOUGHT
                && this.state.gameSettings.participant2 == Participant.BOT) {
                wait(this.state.gameSettings.pauseAfterMove)
                    .then(() => this.makeBotMove(this.state.bot1));
            }
        }
    };

    makeBotMove(bot: Bot) {
        const board = this.state.game.getCurrentBoard();
        const moves = this.state.game.getAvailableMoves();
        const botMovement = bot.makeMove(board, moves);
        this.moveHandler(botMovement);
    }

    setAsyncState = (newState: Partial<State>): Promise<void> => {
        return new Promise((resolve) => {
            this.setState(newState as State, () => {
                resolve();
            });
        });
    };

    makeMove(move: Coordinates) {
        this.state.game.move(move);
        const winner = this.state.game.winner();
    }

    toResult(result: (Piece | Result.DRAW)) {
        if (result == Result.DRAW) {
            return result;
        } else if (result == Piece.NOUGHT) {
            return Result.PLAYER_TWO;
        } else {
            return Result.PLAYER_ONE;
        }
    }

    toPlayerResult(result: (Piece | Result.DRAW), piece: Piece): PlayerResult {
        if (result == Result.DRAW) {
            return PlayerResult.DRAW;
        } else if (result == piece) {
            return PlayerResult.WIN;
        } else {
            return PlayerResult.LOSS;
        }
    }

    resetGame = () => {
        this.setState({game: new Game()}, () => {
            this.proceedGame();
        });
    };

    gameSettingsChangeHandler = (settings: GameSettings) => {
        this.setState({gameSettings: settings},
            () => {
                this.proceedGame();
            });
    };
}

interface State {
    game: Game;
    history: Result[];
    bot1: Bot;
    bot2: Bot;
    gameSettings: GameSettings;
}
