import {ChangeEvent, Component} from 'react';
import GameSettings, {Participant} from '../../model/tic-tac-toe/GameSettings';
import {Switch, TextField} from '@mui/material';

export default class GameSettingsView extends Component<Props, any> {
    render() {
        const settings = this.props.gameSettings;
        return <div className="game-settings">
            <TextField
                label="Pause after game"
                type="number"
                name="pause-after-game"
                value={settings.pauseAfterGame}
                onChange={this.handleSettingsChange}
                size="small"
            />
            <TextField
                label="Pause after move"
                type="number"
                name="pause-after-move"
                value={settings.pauseAfterMove}
                onChange={this.handleSettingsChange}
                size="small"
            />
            <div className="switch"> Player One Bot
                <Switch name="player1" checked={settings.participant1 == Participant.BOT}
                        onChange={this.handleSettingsChange}
                /></div>
            <div className="switch"> Player Two Bot
                <Switch name="player2" checked={settings.participant2 == Participant.BOT}
                        onChange={this.handleSettingsChange}
                /></div>

        </div>;
    }

    handleSettingsChange = (event: ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const currentSettings = this.props.gameSettings;
        let settings = null;
        if (target.name == 'player1') {
            if (currentSettings.participant1 == Participant.BOT) {
                settings = this.props.gameSettings.setParticipant1(Participant.PLAYER);
            } else {
                settings = this.props.gameSettings.setParticipant1(Participant.BOT);
            }
        }
        if (target.name == 'player2') {
            if (currentSettings.participant2 == Participant.BOT) {
                settings = currentSettings.setParticipant2(Participant.PLAYER);
            } else {
                settings = currentSettings.setParticipant2(Participant.BOT);
            }
        }
        if (target.name == 'pause-after-move') {
            settings = currentSettings.setPauseAfterMove(Number(target.value));
        }
        if (target.name == 'pause-after-game') {
            settings = currentSettings.setPauseAfterGame(Number(target.value));
        }
        if (settings != null) {
            this.props.gameSettingsChangeHandler(settings);
        }
    };

}

interface Props {
    gameSettings: GameSettings;
    gameSettingsChangeHandler: (settings: GameSettings) => void;
}
