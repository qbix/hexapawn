import {Component, MouseEvent, ReactNode} from 'react';
import {Piece} from '../../model/tic-tac-toe/Piece';
import Icon from '@mdi/react';
import * as icons from '@mdi/js';
import {format, rgb} from '../../model/shared/FormatUtils';

export default class FieldView extends Component<Props, any> {
    render() {
        const probability = this.props.probability;
        let style = {};
        if (probability != undefined) {
            style = {backgroundColor: rgb(probability)};
        }
        return <div style={style} className="field" onClick={this.props.onClick}>{this.field()}</div>;
    }

    field = (): ReactNode | null => {
        const piece = this.props.piece;
        const probability = this.props.probability;
        if (piece == Piece.NOUGHT) {
            return <Icon className="nought" path={icons.mdiRobotHappy}/>;
        } else if (piece == Piece.CROSS) {
            return <Icon className="cross" path={icons.mdiCircleOutline}/>;
        } else if (probability != undefined) {
            return <span>{format(probability)}</span>;
        } else {
            return null;
        }
    };
}

interface Props {
    piece: Piece | null;
    probability?: number;
    onClick: (event: MouseEvent) => void;
}
