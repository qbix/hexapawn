import React, {Component} from 'react';
import {Box, RadioGroup, Tab, Tabs} from '@mui/material';
import TabPanel from '../shared/TabPanel';
import BotView from './BotView';
import BotSettings from '../../model/tic-tac-toe/bot/BotSettings';
import {BoardEntry} from '../../model/tic-tac-toe/bot/Bot';
import GameSettings from '../../model/tic-tac-toe/GameSettings';
import GameSettingsView from './GameSettingsView';
import {RadioRounded} from '@mui/icons-material';

export default class SettingsView extends Component<Props, State> {
    state = {
        selectedTab: 0
    };

    render() {
        return (
            <Box className="settings-view">
                <GameSettingsView
                    gameSettingsChangeHandler={this.props.gameSettingsChangeHandler}
                    gameSettings={this.props.gameSettings}/>
                <Box>
                    <Tabs value={this.state.selectedTab} onChange={this.handleChange}>
                        <Tab label="Bot1" {...a11yProps(0)}/>
                        <Tab label="Bot2" {...a11yProps(1)}/>
                    </Tabs>
                </Box>
                <Box>
                    <TabPanel index={0} value={this.state.selectedTab}>
                        <BotView botSetings={this.props.bot1Settings} boardEntries={this.props.bot1BoardEntries}
                                 botSettingsChangeHandler={this.props.bot1SettingsChangeHandler}/>
                    </TabPanel>
                    <TabPanel index={1} value={this.state.selectedTab}>
                        <BotView botSetings={this.props.bot2Settings} boardEntries={this.props.bot2BoardEntries}
                                 botSettingsChangeHandler={this.props.bot2SettingsChangeHandler}/>
                    </TabPanel>
                </Box>
            </Box>
        );
    }

    handleChange = (event: React.SyntheticEvent, newValue: number) => {
        this.setState({selectedTab: newValue});
    };
}

interface Props {
    gameSettings: GameSettings,
    bot1Settings: BotSettings,
    bot2Settings: BotSettings,
    bot1BoardEntries: BoardEntry[]
    bot2BoardEntries: BoardEntry[]
    bot1SettingsChangeHandler: (botSettings: BotSettings) => void;
    bot2SettingsChangeHandler: (botSettings: BotSettings) => void;
    gameSettingsChangeHandler: (gameSettings: GameSettings) => void;
}

interface State {
    selectedTab: number;
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

