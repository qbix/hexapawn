import {Component, ReactNode} from 'react';
import {Area, AreaChart, Legend, XAxis, YAxis} from 'recharts';
import {Result} from '../../model/tic-tac-toe/Result';
import {chartData, total} from '../../model/tic-tac-toe/StatsCalc';

export default class StatisticsView extends Component<Props, any> {
    render() {
        return <div className="statistics-view">
            <div className="table">
                <table>
                    <thead>
                    <tr>
                        <th>X</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.rows()}
                    </tbody>
                </table>
            </div>
            {this.chart()}
        </div>;
    }

    rows(): ReactNode {
        return [this.row(Result.PLAYER_TWO), this.row(Result.PLAYER_ONE), this.row(Result.DRAW)];
    }

    row(result: Result): ReactNode {
        return <tr key={result}>
            <td>{result}</td>
            <td>{this.format(total(this.props.history, result))}</td>
        </tr>;
    }

    format(ratio: number): String {
        return (Math.round(ratio * 10000) / 100) + '%';
    }

    chart(): ReactNode {
        return <div className="chart-box">
            <AreaChart data={chartData(this.props.history)} width={400} height={400}>
                <Area dataKey="Player" stackId="stack" fill="green" stroke="green"/>
                <Area dataKey="Draw" stackId="stack" fill="orange" stroke="orange"/>
                <Area dataKey="Bot" stackId="stack" fill="red" stroke="red"/>
                <XAxis dataKey="n" allowDataOverflow={false} allowDecimals={false} scale="linear"
                       type="number"/>
                <YAxis domain={[0, 1]}/>
                <Legend/>
            </AreaChart>
        </div>;
    }
}

interface Props {
    history: Result[];
}
