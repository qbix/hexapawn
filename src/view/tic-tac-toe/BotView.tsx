import React, {Component, ReactNode} from 'react';
import {BoardEntry} from '../../model/tic-tac-toe/bot/Bot';
import BoardView from './BoardView';
import Moves from '../../model/tic-tac-toe/Moves';
import {TextField} from '@mui/material';
import BotSettings from '../../model/tic-tac-toe/bot/BotSettings';
import {compareBoardEntries} from '../../model/tic-tac-toe/BoardEntryComparator';

export default class BotView extends Component<Props, any> {
    render() {
        return (
            <div>
                <div className="header">Bot</div>
                {this.settings()}
                {this.botBoards()}
            </div>
        );
    }

    settings(): ReactNode {
        const settings = this.props.botSetings;
        return <div className="settings">
            <div className="header-2">Settings</div>
            <div className="settings-controls">
                <TextField
                    label="Win reward"
                    type="number"
                    value={settings.win}
                    onChange={this.boardSettingsChange}
                    name="win"
                    inputProps={{
                        step: '0.1'
                    }}
                    size="small"
                />
                <TextField
                    label="Loss reward"
                    type="number"
                    value={settings.loss}
                    onChange={this.boardSettingsChange}
                    name="loss"
                    inputProps={{
                        step: '0.1'
                    }}
                    size="small"
                />
                <TextField
                    label="Draw reward"
                    type="number"
                    value={settings.draw}
                    onChange={this.boardSettingsChange}
                    name="draw"
                    inputProps={{
                        step: '0.1'
                    }}
                    size="small"
                />
            </div>
        </div>;
    }

    botBoards(): ReactNode {
        return <div className="known-boards">
            <div className="header-2">Known boards ({this.props.boardEntries.length})</div>
            <div className="boards">
                {this.props.boardEntries
                    .sort(compareBoardEntries)
                    .map((entry, index) => this.boardEntry(entry, index))
                }</div>
        </div>;
    }

    boardEntry(entry: BoardEntry, index: number): ReactNode {
        return <BoardView
            key={index}
            board={entry.getBoard()}
            probabilities={entry.getMoves()}
            availableMoves={new Moves([])}
            moveHandler={() => {
            }}/>;
    }

    boardSettingsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        const settings = this.props.botSetings;
        switch (name) {
            case 'loss':
                this.props.botSettingsChangeHandler(settings.setLossReward(Number(value)));
                break;
            case 'win':
                this.props.botSettingsChangeHandler(settings.setWinReward(Number(value)));
                break;
            case 'draw':
                this.props.botSettingsChangeHandler(settings.setDrawReward(Number(value)));
        }
    };
}

interface Props {
    botSetings: BotSettings;
    boardEntries: BoardEntry[];
    botSettingsChangeHandler: (botSettings: BotSettings) => void;
}
