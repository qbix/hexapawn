import React, {Component} from 'react';
import Hexapawn from './hexapawn/Hexapawn';
import {Box, Tabs, Tab} from '@mui/material';
import './MainView.scss';
import TicTacToe from './tic-tac-toe/TicTacToe';

export default class MainView extends Component<any, State> {
    state = {
        selectedTab: 0
    };

    render() {
        return <div className="main-view">
            <Box className="game-container">
                <Box>
                    <Tabs value={this.state.selectedTab} onChange={this.handleChange}>
                        <Tab label="Hexapawn" {...a11yProps(0)}/>
                        <Tab label="TicTacToe" {...a11yProps(1)}/>
                    </Tabs>
                </Box>
                <Box>
                    <TabPanel value={this.state.selectedTab} index={0}>
                        <Hexapawn/>
                    </TabPanel>
                    <TabPanel value={this.state.selectedTab} index={1}>
                        <TicTacToe/>
                    </TabPanel></Box>
            </Box>
            <div className="popper">version: {process.env['REACT_APP_VERSION']}</div>
        </div>;
    }

    handleChange = (event: React.SyntheticEvent, newValue: number) => {
        this.setState({selectedTab: newValue});
    };

}

interface State {
    selectedTab: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}
