import React, {Component, ReactNode} from 'react';
import {Piece} from '../../model/hexapawn/Piece';
import {Icon} from '@mdi/react';
import {mdiChessPawn} from '@mdi/js';
import {MouseEvent} from 'react';

export default class FieldView extends Component<Props, any> {

    constructor(props: Props, options: any) {
        super(props, options);
    }

    render() {
        let className = '';
        if (this.props.selected) {
            className += ' selected';
        }
        if (this.props.available) {
            className += ' available';
        }
        return <div onClick={this.props.onClick} className={`field ${className}`}>{this.field()}</div>;
    }

    field = (): ReactNode | null => {
        const field = this.props.field;
        if (field) {
            const colorClass = this.props.field === Piece.WHITE ? 'white' : 'black';
            return <Icon className={`pawn ${colorClass}`} path={mdiChessPawn}/>;
        } else {
            return null;
        }
    };
}

interface Props {
    field: Piece | null;
    selected: boolean;
    available: boolean;
    onClick: (event: MouseEvent) => void;
}
