import React, {Component} from 'react';
import {BoardEntry} from '../../../model/hexapawn/bot/Bot';
import BotMovement from '../../../model/hexapawn/bot/BotMovement';
import BotSettings from '../../../model/hexapawn/bot/BotSettings';
import SettingsView from './SettingsView';
import BoardsView from './BoardsView';

export default class BotView extends Component<Props, any> {
    render() {
        return <div className="bot">
            <div className="header">Bot</div>
            <SettingsView
                settings={this.props.botSettings}
                settingChangeHandler={this.props.botSettingsHandler}/>
            <BoardsView
                boardEntries={this.props.boardEntries}
                botMoves={this.props.botMoves}
            />
        </div>;
    }
}

interface Props {
    boardEntries: BoardEntry[];
    botSettings: BotSettings;
    botSettingsHandler: (settings: BotSettings) => void;
    botMoves: BotMovement[];
}
