import React, {Component, ReactNode} from 'react';
import BoardView from '../BoardView';
import Moves from '../../../model/hexapawn/Moves';
import Coordinates from '../../../model/shared/Coordinates';
import {BoardEntry, MoveEntry} from '../../../model/hexapawn/bot/Bot';
import Board from '../../../model/shared/Board';
import {Piece} from '../../../model/hexapawn/Piece';
import BotMovement from '../../../model/hexapawn/bot/BotMovement';
import {format, rgb} from '../../../model/shared/FormatUtils';
import Movement from '../../../model/shared/Movement';

export default class BoardsView extends Component<Props, any> {
    render() {
        return (
            <div className="bot-boards-panel">
                <div className="header-2">Known boards ({this.props.boardEntries.length})</div>
                {this.botBoards()}
            </div>
        );
    }

    botBoards = (): ReactNode => {
        const boards = this.props.boardEntries
            .map((entry, index) => <div
                key={index}
                className={`board-entry ${this.isBoardSelected(entry.board) ? 'selected' : ''}`}>
                <BoardView
                    board={entry.board}
                    availableMoves={new Moves(new Map<Coordinates, Coordinates[]>())}
                    moveHandler={() => {
                    }}/>
                {this.botMoves(entry)}
            </div>);
        return <div className="bot-boards">{boards}</div>;
    };

    isBoardSelected = (board: Board<Piece>): boolean => {
        return this.props.botMoves.filter(bd => bd.board.equals(board)).length > 0;
    };

    isMoveSelected = (board: Board<Piece>, move: Movement): boolean => {
        if (!this.isBoardSelected(board)) {
            return false;
        }
        const thisBoardMoves = this.props.botMoves.filter(mv => mv.board.equals(board))[0];
        return thisBoardMoves.move.equals(move);
    };

    botMoves = (entry: BoardEntry): ReactNode => {
        return (
            <div className="moves">
                <table>
                    <thead>
                    <tr>
                        <th>Move</th>
                        <th>Ratio</th>
                    </tr>
                    </thead>
                    <tbody>
                    {entry.moves
                        .map(move => this
                            .moveView(
                                move,
                                entry.moves.map(move => move.ratio)
                                    .reduce((r1, r2) => r1 + r2),
                                this.isMoveSelected(entry.board, move.move)
                            ))
                    }
                    </tbody>
                </table>
            </div>);
    };

    moveView = (move: MoveEntry, total: number, selected: boolean): ReactNode => {
        return (
            <tr
                key={`${this.mapCoordinates(move.move.from)} ➜ ${this.mapCoordinates(move.move.to)}`}
                className={`move ${selected ? 'selected' : ''}`}>
                <td>{`${this.mapCoordinates(move.move.from)} ➜ ${this.mapCoordinates(move.move.to)}`}</td>
                <td style={{backgroundColor: rgb(move.ratio / total)}}>{this.ratioString(move, total)}</td>
            </tr>
        );
    };

    mapCoordinates = (coords: Coordinates): string => {
        let row;
        switch (coords.column) {
            case 0:
                row = 'a';
                break;
            case 1:
                row = 'b';
                break;
            case 2:
                row = 'c';
                break;
        }
        let col = 3 - coords.row;
        return `${row}${col}`;
    };

    ratioString = (move: MoveEntry, total: number): string => {
        if (total <= 0) {
            return '0 %';
        } else {
            return format(move.ratio / total);
        }
    };
}

interface Props {
    boardEntries: BoardEntry[];
    botMoves: BotMovement[];
}
