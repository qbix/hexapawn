import React, {Component} from 'react';
import {Checkbox, FormControlLabel, TextField} from '@mui/material';
import BotSettings from '../../../model/hexapawn/bot/BotSettings';

export default class SettingsView extends Component<Props, any> {
    render() {
        const settings = this.props.settings;
        const label = {
            inputProps: {'aria-label': 'Test'}
        };
        return (
            <div className="settings">
                <div className="header-2">Settings</div>
                <div className="settings-controls">
                    <TextField
                        label="Reward"
                        type="number"
                        onChange={this.boardSettingsChange}
                        value={settings.reward}
                        name="reward"
                        inputProps={{
                            step: '0.1'
                        }}
                        size={'small'}/><br/>
                    <TextField
                        label="Punishment"
                        type="number"
                        inputProps={{
                            step: '0.1'
                        }}
                        onChange={this.boardSettingsChange}
                        value={settings.punishment}
                        name="punishment"
                        size={'small'}/>
                    <FormControlLabel control={<Checkbox {...label} checked={settings.symmetry}
                                                         onChange={this.boardSettingsChange}/>} label="Track symmetry"/>

                </div>
            </div>
        );
    }

    boardSettingsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const settings = this.props.settings;
        const {name, value} = event.target;
        if (name == 'punishment') {
            this.props.settingChangeHandler(settings.setPunishment(Number(value)));
        } else if (event.target.name == 'reward') {
            this.props.settingChangeHandler(settings.setReward(Number(value)));
        } else {
            this.props.settingChangeHandler(settings.toggleSymmetry());
        }
    };
}

interface Props {
    settings: BotSettings;
    settingChangeHandler: (settings: BotSettings) => void;
}
