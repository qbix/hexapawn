import React, {Component} from 'react';
import './Hexapawn.scss';
import StatisticsView from './StatisticsView';
import Game from '../../model/hexapawn/Game';
import {Piece} from '../../model/hexapawn/Piece';
import Bot, {defaultBot} from '../../model/hexapawn/bot/Bot';
import BotView from './bot/BotView';
import BoardView from './BoardView';
import {Button} from '@mui/material';
import Movement from '../../model/shared/Movement';
import BotSettings from '../../model/hexapawn/bot/BotSettings';

class Hexapawn extends Component<any, State> {
    state = {
        history: [],
        game: new Game(defaultBot),
        bot: defaultBot
    };

    render() {
        return (
            <div className="hexapawn">
                <span className="left"><StatisticsView history={this.state.history}/></span>
                <span className="center">
                    <BoardView board={this.state.game.getCurrentBoard()} availableMoves={this.state.game.availableMoves}
                               moveHandler={this.handleMove}/>
                    {this.state.game.getWinner() && <div>Winner is: {this.state.game.getWinner()}
                        <Button onClick={this.resetGame}>New game</Button>
                    </div>}
                </span>
                <span className="right">
                    <div className="rules">
                        <div className="header">Rules</div>
                        <div className="header-2">Movement</div>
                        <span>The pawn moves 1 square forward, and captures 1 square diagonally</span>
                        <div className="header-2">Win condition</div>
                        <ul>
                            <li>Pawn passes to opposite side</li>
                            <li>Opponent lost all pawns</li>
                            <li>Opponent has no legal move left</li>
                        </ul>
                    </div>
                    <BotView
                        botSettings={this.state.bot.settings}
                        boardEntries={this.state.bot.boardEntries}
                        botMoves={this.state.game.botMovements}
                        botSettingsHandler={this.handleBotSettingsChange}/>
                </span>
            </div>
        );
    }

    resetGame = () => {
        this.setState({game: new Game(defaultBot)});
    };

    handleMove = (move: Movement) => {
        const game = this.state.game.move(move);
        this.setState({
            game: game
        }, this.handleWin);
    };

    handleWin = () => {
        const winner = this.state.game.getWinner();
        if (winner) {
            this.state.bot.learn(this.state.game.botMovements, winner);
            this.setState({
                history: [...this.state.history, winner]
            });
        }
    };

    handleBotSettingsChange = (settings: BotSettings): void => {
        this.state.bot.settings = settings;
        this.setState({});
    };
}

interface State {
    history: Piece[];
    game: Game;
    bot: Bot;
}

export default Hexapawn;
