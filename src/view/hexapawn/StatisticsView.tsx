import {Component} from 'react';
import {Piece} from '../../model/hexapawn/Piece';
import {currentStreak, getLast, getMoving10Average, getTotal, getTotalRatio} from '../../model/hexapawn/StatsCounter';
import {Legend, Line, LineChart, XAxis, YAxis} from 'recharts';

export default class StatisticsView extends Component<Props, any> {
    render() {
        return (
            <div className="stats">
                <div className="table">
                    <table>
                        <thead>
                        <tr>
                            <th>X</th>
                            <th>White</th>
                            <th>Black</th>
                            <th>Ratio</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.rowFor(this.props.history, 'Total')}
                        {this.rowFor(getLast(this.props.history, 10), 'Last 10')}
                        {this.rowFor(getLast(this.props.history, 20), 'Last 20')}
                        <tr>
                            <th>Current streak</th>
                            <td colSpan={3}>{currentStreak(this.props.history)}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div className="chart-box">
                    <LineChart className="chart" width={400} height={400}
                               data={getMoving10Average(this.props.history)}>
                        <Line dataKey="avg10" type="natural" dot={false} stroke="green"/>
                        <Line dataKey="avg20" type="natural" dot={false} stroke="red"/>
                        <Line dataKey="avgTotal" type="natural" dot={false} stroke="blue"/>
                        <XAxis dataKey="n" allowDataOverflow={false} allowDecimals={false} scale="linear"
                               type="number"/>
                        <YAxis domain={[0, 100]}/>
                        <Legend/>
                    </LineChart>
                </div>
            </div>
        );
    }

    rowFor = (history: Piece[], name: string) => (
        <tr>
            <th>{name}</th>
            <td>{getTotal(history, Piece.WHITE)}</td>
            <td>{getTotal(history, Piece.BLACK)}</td>
            <td>{Math.round(getTotalRatio(history) * 10000) / 100}%</td>
        </tr>);
}

interface Props {
    history: Piece[];
}
