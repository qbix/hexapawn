import React, {Component, ReactNode} from 'react';
import FieldView from './FieldView';
import Moves from '../../model/hexapawn/Moves';
import {Piece} from '../../model/hexapawn/Piece';
import Board from '../../model/shared/Board';
import Coordinates from '../../model/shared/Coordinates';
import Movement from '../../model/shared/Movement';

export default class BoardView extends Component<Props, State> {
    state = {} as State;

    render() {
        return (<div className="board">{this.fields()}</div>);
    }

    fields = (): ReactNode[] => {
        const fields = this.props.board.fields;
        const fieldViews: ReactNode[] = [];
        for (let x = 0; x < fields[0].length; x++) {
            for (let y = 0; y < fields.length; y++) {
                const coordinates = new Coordinates(x, y);
                const selectedCoords = this.state.selectedCoords;
                const selected = coordinates.equals(selectedCoords);
                const available = this.availableCoords()
                    .filter(avail => avail.equals(coordinates))
                    .length > 0;
                fieldViews.push(<FieldView key={Math.random()} selected={selected} available={available}
                                           onClick={this.clickField(x, y)}
                                           field={fields[x][y]}/>);
            }
        }
        return fieldViews;
    };

    availableCoords = (): Coordinates[] => {
        if (!this.state.selectedCoords) {
            return [];
        } else {
            return this.props.availableMoves.getMovesFrom(this.state.selectedCoords)
                .map(move => move.to);
        }
    };

    selectableCoords = (): Coordinates[] => {
        return this.props.availableMoves.getValidOrigins();
    };

    clickField = (row: number, column: number) => () => {
        const coords = new Coordinates(row, column);
        const selected = this.state.selectedCoords;
        if (selected === undefined) {
            if (this.isSelectable(coords)) {
                this.selectField(coords);
            }
        }
        if (coords.equals(selected)) {
            this.deselectField();
        }
        if (this.isAvailable(coords) && selected) {
            this.move(new Movement(selected, coords));
        }
    };

    isAvailable = (coords: Coordinates): boolean => {
        return this.availableCoords()
            .filter(a => coords.equals(a))
            .length > 0;
    };

    isSelectable = (coords: Coordinates): boolean => {
        return this.selectableCoords()
            .filter(selectable => selectable.equals(coords))
            .length > 0;
    };

    selectField = (coords: Coordinates) => {
        this.setState({
            selectedCoords: coords
        });
    };

    deselectField = () => {
        this.setState({selectedCoords: undefined});
    };

    move = (move: Movement) => {
        this.props.moveHandler(move);
        this.setState({selectedCoords: undefined});
    };
}

interface Props {
    board: Board<Piece>;
    availableMoves: Moves;
    moveHandler: (move: Movement) => void;
}

interface State {
    selectedCoords?: Coordinates;
}
