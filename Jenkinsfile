pipeline {
  agent {
    label 'nodejs-16'
  }
  stages {
    stage("Set environment variable") {
      steps {
        script {
          env.APP_NAME = 'hexapawn'
          if ("$GIT_BRANCH" == "master") {
            env.ENVIRONMENT = 'prd'
          } else {
            env.ENVIRONMENT = 'dev'
          }

          switch (env.ENVIRONMENT) {
            case 'dev':
              env.DOCKER_REGISTRY = env.dockerRepoSnapshot
              break
            case 'prd':
              env.DOCKER_REGISTRY = env.dockerRepoRelease
              break
          }
        }
      }
    }
    stage('Calculate version') {
      steps {
        sshagent(['credentials-bitbucket-private-key']) {
          script {
            env.version = sh(returnStdout: true, script: './version').trim()
            def text = readFile file: ".env"
            text = text.replaceAll("%version%", "${env.version}")
            writeFile file: ".env", text: text
            stash includes: ".env", name: 'env'
            env.DOCKER_IMAGE = "${env.DOCKER_REGISTRY}/${env.APP_NAME}:${env.version}"
          }
          echo env.version
        }
      }
    }
    stage('Build') {
      steps {
        container('node-js') {
          unstash 'env'
          sh 'npm install'
          sh 'npm install react-scripts@3.4.1 -g'
          sh 'npm run build'
          stash includes: 'build/**/*', name: 'build'
        }
      }
    }
    stage('Docker image') {
      agent {
        kubernetes {
          label 'containers'
          defaultContainer 'containers-manager'
        }
      }
      when {
        environment name: 'ENVIRONMENT', value: 'dev'
        beforeAgent true
      }
      stages {
        stage('Build docker') {
          steps {
            unstash 'build'
            unstash 'env'
            sh label: "build docker", script: "/var/run/docker/docker build . " +
                "-t ${env.DOCKER_IMAGE}"
          }
        }
        stage('Push docker image') {
          steps {
            sh label: "login to docker", script: "docker login -u ${env.dockerUser} -p ${env.dockerPassword} ${env.DOCKER_REGISTRY}"
            sh label: "push docker image", script: "docker push ${env.DOCKER_IMAGE}"
          }
        }
      }
    }
    stage('Push version tag') {
      when {
        environment name: 'ENVIRONMENT', value: 'prd'
      }
      steps {
        sshagent(['credentials-bitbucket-private-key']) {
          sh label: 'push tag change', script: 'git push origin --tags'
        }
        addShortText background: '#fffccc',
            borderColor: '#5b5b5b',
            color: '#5b5b5b',
            text: "${env.version}",
            border: 1
      }
    }
    stage('Deploy to k8s') {
      when {
        environment name: 'ENVIRONMENT', value: 'dev'
      }
      steps {
        build job: 'k8s-deployment/master',
            parameters: [
                string(name: 'env', value: env.ENVIRONMENT),
                string(name: 'appName', value: env.APP_NAME),
                string(name: 'appVersion', value: env.version)]
        addShortText background: '#fffccc',
            borderColor: '#5b5b5b',
            color: '#5b5b5b',
            text: env.ENVIRONMENT,
            border: 1
      }
    }

    stage("Deploy to nazwa.pl") {
      agent {
        label 'ssh-tools'
      }
      options {
        skipDefaultCheckout()
      }
      when {
        environment name: 'ENVIRONMENT', value: 'prd'
        beforeAgent true
      }
      steps {
        container('ssh-tools') {
          withCredentials([usernamePassword(credentialsId: 'credentials-nazwa-pl-user',
              usernameVariable: 'NAZWA_PL_USERNAME',
              passwordVariable: 'NAZWA_PL_PASSWORD')]) {
            unstash 'build'
            unstash 'env'
            sh label: 'rename build directory',
                script: 'mv build hexapawn'
            sh label: 'remove files',
                script: "sshpass -p '${NAZWA_PL_PASSWORD}' ssh -o StrictHostKeyChecking=no ${NAZWA_PL_USERNAME}@qbixplze.nazwa.pl" +
                    " rm /home/qbixplze/ftp/hexapawn/* -r -f"
            sh label: 'add new version',
                script: "sshpass -p '${NAZWA_PL_PASSWORD}' scp -o StrictHostKeyChecking=no -r " +
                    "./hexapawn ${NAZWA_PL_USERNAME}@qbixplze.nazwa.pl:hexapawn/.."
          }
        }
      }
    }
  }
}
